# Codeable Aero Theme
Custom Responsive Wordpress Theme based on HTML5 Blank Theme & Advanced Project Workflow with Gulp, Sass & Browsersync.



The folders that you are working on are the following:

  - assets/css (the folder of the source .scss file).
  - assets/js (all your javascript and jQuery files).
  - assets/img (where all your images reside).
  - lib/ (All custom functions)


### Technologies

This project uses a number of open source projects to work properly:

* [Gulp](http://gulpjs.com/) - The streaming build system
* [Sass](http://sass-lang.com/) - Awesome css preproccessor
* [BrowserSync](https://browsersync.io/) - Time-saving synchronised browser testing
* [Bootstrap](http://getbootstrap.com/) - Popular grid and css system

Along with many Gulp libraries (these can be seen in `package.json`).

---

### Development

You need Gulp installed globally:

```sh
$ npm i -g gulp
```
And then run the following commands:
```sh
$ (sudo) npm install
$ gulp 
```


Your browser will automatically be opened and directed to the browser-sync proxy address
* To prepare assets for build theme file in ZIP format, run the `gulp build` 


Enjoy!


### Main Theme

All files that are located within the main theme folder are structured in the following manner:

```
/assets
  css/      (the main folder where all .scss file are stored)
/assets
  js/       (the main folder where all .js file are stored)
/assets
  img/      (the main folder where all image files are stored)
/lib        (the main folder where all custom functions are stored)
home.php    (the example file used for the frontpage design)
 
```


### SASS

SASS, standing for 'Syntactically Awesome Style Sheets', is a CSS extension language adding things like extending, variables, and mixins to the language. This boilerplate provides a barebones file structure for your styles, with explicit imports into `app/styles/main.scss`. A Gulp task (discussed later) is provided for compilation and minification of the stylesheets based on this file.

---

### Gulp

Gulp is a "streaming build system", providing a very fast and efficient method for running your build tasks.

##### Web Server

Gulp is used here to provide a very basic node/Express web server for viewing and testing your application as you build. It serves static files from the `build/` directory, leaving routing up to AngularJS. All Gulp tasks are configured to automatically reload the server upon file changes. The application is served to `localhost:3002` once you run the `gulp` task. To take advantage of the fast live reload injection provided by browser-sync, you must load the site at the proxy address (within this boilerplate will by default be `localhost:3000`). To change the settings related to live-reload or browser-sync, you can access the UI at `localhost:3001`.

##### Styles

Just one plugin is necessary for processing our SASS files, and that is `gulp-sass`. This will read the `main.scss` file, processing and importing any dependencies and then minifying the result. This file (`main.css`) is placed inside the directory `/build/css/`.

- **gulp-autoprefixer:** Gulp is currently configured to run autoprefixer after compiling the scss.  Autoprefixer will use the data based on current browser popularity and property support to apply prefixes for you. Autoprefixer is recommended by Google and used in Twitter, WordPress, Bootstrap and CodePen.

##### Watching files

All of the Gulp processes mentioned above are run automatically when any of the corresponding files in the root theme directory are changed, and this is thanks to our Gulp watch tasks. Running `gulp` will begin watching all of these files, while also serving to `localhost:3002`, and with browser-sync proxy running at `localhost:3000` (by default).

##### Production ZIP file

Just as there is the `gulp` task for development, there is also a `gulp build` task for putting your project into a production-ready ZIP file. This will run each of the tasks. This deploy task can be fleshed out to automatically ZIP your production-ready site.


### Installation

In order to run the project properly you need to do the following (2 different ways):

#### 1. Ready-made backup:

- Create a folder in your web-server, eg. htdocs, public_html etc.
- Create a database and a database user with full priviledges to that database
- Take the backup zip file of the whole project and put it inside the main project's folder along with the importbuddy.php
- Open a web browser and type the path of that project appending the /importbuddy.php eg. http://localhost/codeable-aero/importbuddy.php
- Give the password 'codeable' and run step-by-step the process and give all the database credentials.
- Done


#### 2. Manual Installation:

- Take the theme zip file and unzip it in the themes folder 
- Activate the theme through theme section inside WP admin
- TGM Plugin will inform you for some necessary plugin installation. Install and activate those plugins.
- Head to Menu Tools -> Import and run Wordpress importer
- Import advanced-custom-field-export.xml 
- Add 3 Posts in WP Admin
- Add Testimonials keeping in mind that you need to add all fields (Title, Editor, Featured image, Author name, Author position & Author company)
- Add Clients - Only title ad Featured image should be added
- Header to the frontpage



### Mechanisms used to structure the frontpage

- Hero Section - Static section with image implemented via css background property both for normal and retina screens
- We are aero - Static section
- Latest Posts Section - Used get_posts to fetch latest 3 posts. Used css animations and transitions to make this hover effect
- Why Hire Us Section - Static section with .svg images used for each 4 icons
- Clients Section - Used WP_Query to fetch Custom Post Types Clients. Used css opacity for hover state on brand icons
- From our clients section - Used WP_Query to fetch Custom Post Type Testimonials. For the slider implementation we used jQuery Slick Carousel.
For the image we used the featured image and for the blockquote we used the editor. All the other fields (Author Name, Author Position, Author Company) are Advanced Custom fields.
- How was that - Static section
- Both Header and Footer menus - Used wp_nav_menu to add both menus
- Footer logo - Added #top on click with smooth scolling behaviour


### Ways to implement retina-ready images

- For the logo (header & footer) and for the "Why Hire Us" iconography I chose .svg files instead of the @2x.png files because of the fact that they are lighter and more managable due to the fact that they are vector rather than the raster .png file. SO we can resize them without losing detail.
All major browsers support svg except IE8 (http://caniuse.com/#search=svg). In order to change the svg to png we used Modernizr library and created a custom JS where if no SVG is supported, then we replace .svg suffix with .png
- For the main hero image we used css background property to target the normal image. To target retina devices we used specific media query and we call @2x.png image
- For the "Clients" section and "From our Clients" section, due to the fact that we let the user to handle the images through the Wordpress dashboard, we used retina.js library. What the user needs to do is after adding the normal image to the Media Library to add the retina image also. Then the retina library will try to find if there is a @2x version of that image. If there is then it serves the @2x version of this image 




