<!-- footer -->
<footer class="footer" role="contentinfo">

    <a href="#top" class="footer-logo">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo-footer.svg" alt="<?php bloginfo('name'); ?>">
    </a>
    <!-- copyright -->
    <p class="copyright">
        All rights reserved, &copy; <?php echo date('Y'); ?>  <span class="theme-name"><?php bloginfo('name'); ?></span> inc.
    </p>
    <!-- /copyright -->
    <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container_class' => 'footer-menu-wrapper'  ) ); ?>
    
</footer>
<!-- /footer -->

<?php wp_footer(); ?>

</div>
<!-- /wrapper -->

</body>
</html>