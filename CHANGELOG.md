# Changelog


### Version 1.0.0 
- First version
- CSS, JS, PHP and Watch Routines
- BrowserSync
- LineEndings
- TASK: Image optimization `gulp images`
- TASK: WP POT Translation file generation `gulp translate`