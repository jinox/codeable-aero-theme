/**
 * Custom JS.
 *
 * Custom JS scripts.
 *
 * @since 1.0.0
 */

jQuery(document).ready(function($){
    // Configuration of testimonials slider
    $('.testimonials-wrapper').slick({
        autoplay: true,
        adaptiveHeight: true,
        dots: true,
        arrows: false,
        autoplaySpeed: 5000,
        pauseOnDotsHover: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });
    // Scroll to Specific section based on anchors href attribute
    $("a[href='#top']").click(function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });
});


