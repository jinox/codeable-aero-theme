<?php get_header(); ?>

<main class="main-content" role="main">

    <section class="hero">
        <div class="hero-content">
            <h2><?php _e( 'aepo', THEME_NAME ); ?></h2>
            <h3><?php _e( 'The one and only Aerial Photography Studio', THEME_NAME ); ?></h3>
            <a href="#" class="prev nav-control"></a>
            <a href="#" class="next nav-control"></a>
        </div>
    </section>
    <!-- section -->
    <section class="section" id="about">
        <div class="container">
            <h4><?php _e( 'We are aePo', THEME_NAME ); ?></h4>
            <p><?php  _e( 'Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Donec viverra eleifend lacus, vitae ullamcorper metus. Sed sollicitudin ipsum quis nunc sollicitudin ultrices. Donec euismod scelerisque ligula. Maecenas eu varius risusm eu aliquet arcu. Curabitur fermentum suscipit est, tincidunt mattis lorem luctus id. Donec eget massa a diam condimentum pretium. Allquam erat volutpat. Integer ut tincidunt orci. Etiam tristique, elit ut consectetur lacul.', THEME_NAME ); ?></p>
        </div>
    </section>
    <!-- /section -->

    <!-- section -->
    <section class="section" id="latest-posts">
        <div class="container">
            <h4><?php _e( 'Latest Posts', THEME_NAME ); ?></h4>


            <div class="latest-posts-container">
                <?php
                $args = array( 'posts_per_page' => 3);

                $myposts = get_posts( $args );
                foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
                    <!-- article -->
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                        <figure class="animation-effect">
                            <!-- post thumbnail -->
                            <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                                <?php the_post_thumbnail('large'); // Declare pixel size you need inside the array ?>
                            <?php endif; ?>
                            <!-- /post thumbnail -->
                            <figcaption>
                                <!-- post title -->
                                <h3><?php the_title(); ?></h3>
                                <!-- /post title -->
                                <time class="date" datetime="<?php the_time('j M, Y'); ?>"><?php the_time('j M, Y'); ?></time>
                                <?php echo the_excerpt(); ?>
                                <a href="<?php echo the_permalink(); ?>"></a>
                                <span class="read-more"><?php _e('Read All', THEME_NAME); ?></span>
                            </figcaption>
                        </figure>

                    </article>
                <?php endforeach;
                wp_reset_postdata();?>

            </div>

        </div>
    </section>
    <!-- /section -->

    <!-- section -->
    <section class="section" id="benefits">
        <div class="container">
            <h4><?php _e( 'Why hire us', THEME_NAME ); ?></h4>
            <div class="benefits">
                <div class="block">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/contractor_icn.svg" alt="">
                    <p>Over 200 contracotrs in over 30 states and 20 countries</p>
                </div>
                <div class="block">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/fleet_icn.svg" alt="">
                    <p>Fleet of 5 helicopters and 2 ultralight planes</p>
                </div>
                <div class="block">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/camera_icn.svg" alt="">
                    <p>We use the best gear available in the world</p>
                </div>
                <div class="block">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/helmet_icn.svg" alt="">
                    <p>No accidents since we started the service in 1986</p>
                </div>
            </div>
        </div>
    </section>
    <!-- /section -->

    <!-- section -->
    <section class="section" id="clients">
        <div class="container">
            <h4><?php _e( 'Our clients', THEME_NAME ); ?></h4>

            <?php
            $args = array( 'post_type' => 'clients', 'posts_per_page' => 4 );
            $loop = new WP_Query( $args );
            ?>

            <?php if ($loop->have_posts()) : ?>

                <div class="clients-wrapper">

                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

                        <div class="client">
                            <?php echo the_post_thumbnail(); ?>
                        </div>

                    <?php endwhile; ?>
                </div>
            <?php else: ?>

                <div class="no-entries">
                    <span>There are no client entries found. Please add some <a href="<?php echo admin_url( 'post-new.php?post_type=clients', 'http' ); ?>">here</a></span>
                </div>

            <?php endif; ?>

        </div>
    </section>
    <!-- /section -->

    <!-- section -->
    <section class="section" id="testimonials">
        <div class="container">
            <h4><?php _e( 'From our clients', THEME_NAME ); ?></h4>

            <?php
            $args = array( 'post_type' => 'testimonials', 'posts_per_page' => -1 );
            $loop = new WP_Query( $args );
            ?>

            <?php if ($loop->have_posts()) : ?>

                <div class="testimonials-wrapper">

                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

                        <div class="testimonial">
                            <figure>
                                <?php echo the_post_thumbnail(); ?>
                                <blockquote>
                                    "<?php echo $wpdb->get_var("SELECT post_content FROM $wpdb->posts WHERE ID = $id");?>"
                                </blockquote>
                                <footer>
                                    <cite class="author"><?php echo the_field("testimonial_author_name"); ?></cite>, <cite class="position"><?php echo the_field("testimonial_author_position"); ?></cite> / <cite class="company"><?php echo the_field("testimonial_author_company"); ?></cite>
                                </footer>
                            </figure>

                        </div>

                    <?php endwhile; ?>
                </div>

                <?php else: ?>

                <div class="no-entries">
                    <span>There are no testimonial entries found. Please add some <a href="<?php echo admin_url( 'post-new.php?post_type=testimonials', 'http' ); ?>">here</a></span>
                </div>

                <?php endif; ?>


        </div>
    </section>
    <!-- /section -->

    <!-- section -->
    <section class="section" id="contact">
        <div class="container">
            <h4><?php _e( 'How was that', THEME_NAME ); ?></h4>
            <p><?php  _e( 'Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Donec viverra eleifend lacus, vitae ullamcorper metus. Sed sollicitudin ipsum quis nunc sollicitudin ultrices. Donec euismod scelerisque ligula. Maecenas eu varius risusm eu aliquet arcu. Curabitur fermentum suscipit est, tincidunt mattis lorem luctus id. DOnec eget massa a diam condimentum pretium. Allquam erat volutpat. Integer ut tincidunt orci. Etiam tristique, elit ut consectetur lacul.', THEME_NAME ); ?></p>
            <ul>
                <li><a href="#" class="btn primary">blog</a></li>
                <li><a href="#" class="btn primary">contact</a></li>
            </ul>
        </div>
    </section>
    <!-- /section -->

</main>

<?php get_footer(); ?>

