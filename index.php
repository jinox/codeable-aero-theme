<?php get_header(); ?>

<main class="main-contenr" role="main">
    <!-- section -->
    <section class="container">

        <h1><?php _e( 'Latest Posts', THEME_NAME ); ?></h1>

        <?php get_template_part('assets/views/loop'); ?>

        <?php get_template_part('assets/views/pagination'); ?>

    </section>
    <!-- /section -->
</main>

<?php get_footer(); ?>


