/**
 * Gulpfile.
 *
 * Gulp with WordPress.
 *
 * Implements:
 *      1. Live reloads browser with BrowserSync.
 *      2. CSS: Sass to CSS conversion, error catching, Autoprefixing, Sourcemaps,
 *         CSS minification, and Merge Media Queries.
 *      3. JS: Concatenates & uglifies Vendor and Custom JS files.
 *      4. Images: Minifies PNG, JPEG, GIF and SVG images.
 *      5. Watches files for changes in CSS or JS.
 *      6. Watches files for changes in PHP.
 *      7. Corrects the line endings.
 *      8. InjectCSS instead of browser page reload.
 *      9. Generates .pot file for i18n and l10n.
 *
 * @author Leskas Ioannis (@john_leskas)
 * @version 1.0.0
 */

/**
 * Configuration.
 *
 * Project Configuration for gulp tasks.
 *
 * In paths you can add <<glob or array of globs>>. Edit the variables as per your project requirements.
 */

// START Editing Project Variables.
// Project related.
var project                 = 'Aero', // Project Name.
    projectURL              = 'http://localhost:8888/projects/wordpress-blank/', // Project URL. Could be something like localhost:8888.
    productURL              = './', // Theme/Plugin URL. Leave it like it is, since our gulpfile.js lives in the root folder.
    bower 			= './assets/bower_components/'; // Not truly using this yet, more or less playing right now. TO-DO Place in Dev branch
    build 			= './buildtheme/', // Files that you want to package into a zip go here
    buildInclude 	= [
        // include common file types
        '**/*.php',
        '**/*.html',
        '**/*.css',
        '**/*.js',
        '**/*.svg',
        '**/*.ttf',
        '**/*.otf',
        '**/*.eot',
        '**/*.woff',
        '**/*.woff2',

        // include specific files and folders
        'screenshot.png',

        // exclude files and folders
        '!node_modules/**/*',
        '!assets/bower_components/**/*',
        '!style.css.map',
        '!assets/js/custom/*',
        '!assets/css/patrials/*'

    ],


// Translation related.
text_domain             = 'CODEABLE_AERO', // Your textdomain here.
destFile                = 'CODEABLE_AERO.pot', // Name of the transalation file.
packageName             = 'CODEABLE_AERO', // Package name.
bugReport               = 'http://johnleskas.com/', // Where can users report bugs.
lastTranslator          = 'Leskas Ioannis <your_email@email.com>', // Last translator Email ID.
team                    = 'Mediashare <your_email@email.com>', // Team's Email ID.
translatePath           = './languages', // Where to save the translation files.

// Style related.
styleSRC                = './assets/css/style.scss', // Path to main .scss file.
styleDestination        = './', // Path to place the compiled CSS file.
// Default set to root folder.

// JS Custom related.
jsCustomSRC             = './assets/js/custom/*.js', // Path to JS custom scripts folder.
jsCustomDestination     = './assets/js/', // Path to place the compiled JS custom scripts file.
jsCustomFile            = 'custom', // Compiled JS custom file name.
// Default set to custom i.e. custom.js.

// Images related.
imagesSRC               = './assets/img/raw/**/*.{png,jpg,gif,svg}', // Source folder of images which should be optimized.
imagesDestination       = './assets/img/', // Destination folder of optimized images. Must be different from the imagesSRC folder.

// Watch files paths.
styleWatchFiles         = './assets/css/**/*.scss', // Path to all *.scss files inside css folder and inside them.
customJSWatchFiles      = './assets/js/custom/*.js', // Path to all custom JS files.
projectPHPWatchFiles    = './**/*.php'; // Path to all PHP files.


// Browsers you care about for autoprefixing.
// Browserlist https        ://github.com/ai/browserslist
const AUTOPREFIXER_BROWSERS = [
    'last 2 version',
    '> 1%',
    'ie >= 9',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4',
    'bb >= 10'
];

// STOP Editing Project Variables.

/**
 * Load Plugins.
 *
 * Load gulp plugins and assing them semantic names.
 */
var gulp         = require('gulp'), // Gulp of-course

// CSS related plugins.
    sass         = require('gulp-sass'), // Gulp pluign for Sass compilation.
    minifycss    = require('gulp-uglifycss'), // Minifies CSS files.
    autoprefixer = require('gulp-autoprefixer'), // Autoprefixing magic.
    mmq          = require('gulp-merge-media-queries'), // Combine matching media queries into one media query definition.

// JS related plugins.
    concat       = require('gulp-concat'), // Concatenates JS files
    uglify       = require('gulp-uglify'), // Minifies JS files

// Image related plugins.
    imagemin     = require('gulp-imagemin'), // Minify PNG, JPEG, GIF and SVG images with imagemin.

// Utility related plugins.
    rename       = require('gulp-rename'), // Renames files E.g. style.css -> style.min.css
    lineec       = require('gulp-line-ending-corrector'), // Consistent Line Endings for non UNIX systems. Gulp Plugin for Line Ending Corrector (A utility that makes sure your files have consistent line endings)
    filter       = require('gulp-filter'), // Enables you to work on a subset of the original files by filtering them using globbing.
    sourcemaps   = require('gulp-sourcemaps'), // Maps code in a compressed file (E.g. style.css) back to it’s original position in a source file (E.g. structure.scss, which was later combined with other css files to generate style.css)
    notify       = require('gulp-notify'), // Sends message notification to you
    browserSync  = require('browser-sync').create(), // Reloads browser and injects CSS. Time-saving synchronised browser testing.
    reload       = browserSync.reload, // For manual browser reload.
    wpPot        = require('gulp-wp-pot'), // For generating the .pot file.
    sort         = require('gulp-sort'), // Recommended to prevent unnecessary changes in pot-file.

    rimraf       = require('gulp-rimraf'), // Helps with removing files and directories in our run tasks
    zip          = require('gulp-zip'), // Using to zip up our packaged theme into a tasty zip file that can be installed in WordPress!
    plumber      = require('gulp-plumber'), // Helps prevent stream crashing on errors
    cache        = require('gulp-cache'),
    plugins      = require('gulp-load-plugins')({ camelize: true }),
    ignore       = require('gulp-ignore'), // Helps with ignoring files and directories in our run tasks
    runSequence  = require('gulp-run-sequence'); // Select the sequence you want to run the tasks

/**
 * Task: `browser-sync`.
 *
 * Live Reloads, CSS injections, Localhost tunneling.
 *
 * This task does the following:
 *    1. Sets the project URL
 *    2. Sets inject CSS
 *    3. You may define a custom port
 *    4. You may want to stop the browser from openning automatically
 */
gulp.task( 'browser-sync', function() {
    browserSync.init( {

        // For more options
        // @link http://www.browsersync.io/docs/options/

        // Project URL.
        proxy: projectURL,

        // `true` Automatically open the browser with BrowserSync live server.
        // `false` Stop the browser from automatically opening.
        open: true,

        // Inject CSS changes.
        // Commnet it to reload browser for every CSS change.
        injectChanges: true,

        // Use a specific port (instead of the one auto-detected by Browsersync).
        // port: 7000,

    } );
});


/**
 * Task: `styles`.
 *
 * Compiles Sass, Autoprefixes it and Minifies CSS.
 *
 * This task does the following:
 *    1. Gets the source scss file
 *    2. Compiles Sass to CSS
 *    3. Writes Sourcemaps for it
 *    4. Autoprefixes it and generates style.css
 *    5. Renames the CSS file with suffix .min.css
 *    6. Minifies the CSS file and generates style.min.css
 *    7. Injects CSS or reloads the browser via browserSync
 */
gulp.task('styles', function () {
    gulp.src( styleSRC )
        .pipe( sourcemaps.init() )
        .pipe( sass( {
            errLogToConsole: true,
            outputStyle: 'compact',
            //outputStyle: 'compressed',
            // outputStyle: 'nested',
            // outputStyle: 'expanded',
            precision: 10
        } ) )
        .on('error', console.error.bind(console))
        .pipe( sourcemaps.write( { includeContent: false } ) )
        .pipe( sourcemaps.init( { loadMaps: true } ) )
        .pipe( autoprefixer( AUTOPREFIXER_BROWSERS ) )

        .pipe( sourcemaps.write ( styleDestination ) )
        .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
        .pipe( gulp.dest( styleDestination ) )

        .pipe( filter( '**/*.css' ) ) // Filtering stream to only css files
        .pipe( mmq( { log: true } ) ) // Merge Media Queries only for .min.css version.

        .pipe( browserSync.stream() ) // Reloads style.css if that is enqueued.

        .pipe( rename( { suffix: '.min' } ) )
        .pipe( minifycss( {
            maxLineLen: 10
        }))
        .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
        .pipe( gulp.dest( styleDestination ) )

        .pipe( filter( '**/*.css' ) ) // Filtering stream to only css files
        .pipe( browserSync.stream() )// Reloads style.min.css if that is enqueued.
        .pipe( notify( { message: 'TASK: "styles" Completed! 🌞', onLast: true } ) )
});


/**
 * Task: `customJS`.
 *
 * Concatenate and uglify custom JS scripts.
 *
 * This task does the following:
 *     1. Gets the source folder for JS custom files
 *     2. Concatenates all the files and generates custom.js
 *     3. Renames the JS file with suffix .min.js
 *     4. Uglifes/Minifies the JS file and generates custom.min.js
 */
gulp.task( 'customJS', function() {
    gulp.src( jsCustomSRC )
        .pipe( concat( jsCustomFile + '.js' ) )
        .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
        .pipe( gulp.dest( jsCustomDestination ) )
        .pipe( rename( {
            basename: jsCustomFile,
            suffix: '.min'
        }))
        .pipe( uglify() )
        .pipe( lineec() ) // Consistent Line Endings for non UNIX systems.
        .pipe( gulp.dest( jsCustomDestination ) )
        .pipe( notify( { message: 'TASK: "customJs" Completed! 🌞', onLast: true } ) );
});


/**
 * Task: `images`.
 *
 * Minifies PNG, JPEG, GIF and SVG images.
 *
 * This task does the following:
 *     1. Gets the source of images raw folder
 *     2. Minifies PNG, JPEG, GIF and SVG images
 *     3. Generates and saves the optimized images
 *
 * This task will run only once, if you want to run it
 * again, do it with the command `gulp images`.
 */
gulp.task( 'images', function() {
    gulp.src( imagesSRC )
        .pipe( imagemin( {
            progressive: true,
            optimizationLevel: 3, // 0-7 low-high
            interlaced: true,
            svgoPlugins: [{removeViewBox: false}]
        } ) )
        .pipe(gulp.dest( imagesDestination ))
        .pipe( notify( { message: 'TASK: "images" Completed! 💯', onLast: true } ) );
});


/**
 * WP POT Translation File Generator.
 *
 * * This task does the following:
 *     1. Gets the source of all the PHP files
 *     2. Sort files in stream by path or any custom sort comparator
 *     3. Applies wpPot with the variable set at the top of this file
 *     4. Generate a .pot file of i18n that can be used for l10n to build .mo file
 */
gulp.task( 'translate', function () {
    return gulp.src( projectPHPWatchFiles )
        .pipe(sort())
        .pipe(wpPot( {
            domain        : text_domain,
            destFile      : destFile,
            package       : packageName,
            bugReport     : bugReport,
            lastTranslator: lastTranslator,
            team          : team
        } ))
        .pipe(gulp.dest(translatePath))
        .pipe( notify( { message: 'TASK: "translate" Completed! 💯', onLast: true } ) )

});


/**
 * Clean gulp cache
 */
gulp.task('clear', function () {
    cache.clearAll();
});

/**
 * Clean tasks for zip
 *
 * Being a little overzealous, but we're cleaning out the build folder, codekit-cache directory and annoying DS_Store files and Also
 * clearing out unoptimized image files in zip as those will have been moved and optimized
 */

gulp.task('cleanup', function() {
    return 	gulp.src(['./assets/bower_components', '**/.sass-cache','**/.DS_Store'], { read: false }) // much faster
        .pipe(ignore('node_modules/**')) //Example of a directory to ignore
        .pipe(rimraf({ force: true }))
    // .pipe(notify({ message: 'Clean task complete', onLast: true }));
});
gulp.task('cleanupFinal', function() {
    return 	gulp.src(['./assets/bower_components','**/.sass-cache','**/.DS_Store', build], { read: false }) // much faster
        .pipe(ignore('node_modules/**')) //Example of a directory to ignore
        .pipe(rimraf({ force: true }))
    // .pipe(notify({ message: 'Clean task complete', onLast: true }));
});

/**
 * Build task that moves essential theme files for production-ready sites
 *
 * buildFiles copies all the files in buildInclude to build folder - check variable values at the top
 * buildImages copies all the images from img folder in assets while ignoring images inside raw folder if any
 */

gulp.task('buildFiles', function() {
    return 	gulp.src(buildInclude)
        .pipe(gulp.dest(build))
        .pipe(notify({ message: 'Copy from buildFiles complete', onLast: true }));
});


/**
 * Images
 *
 * Look at src/images, optimize the images and send them to the appropriate place
 */
gulp.task('buildImages', function() {
    return 	gulp.src(['assets/img/**/*', '!assets/images/raw/**'])
        .pipe(gulp.dest(build+'assets/img/'))
        .pipe(plugins.notify({ message: 'Images copied to buildTheme folder', onLast: true }));
});

/**
 * Zipping build directory for distribution
 *
 * Taking the build folder, which has been cleaned, containing optimized files and zipping it up to send out as an installable theme
 */
gulp.task('buildZip', function () {
    // return 	gulp.src([build+'/**/', './.jshintrc','./.bowerrc','./.gitignore' ])
    return 	gulp.src(build+'/**/')
        .pipe(zip(project+'.zip'))
        .pipe(gulp.dest('./'))
        .pipe(notify({ message: 'Zip task complete', onLast: true }));
});


/**
 * Watch Tasks.
 *
 * Watches for file changes and runs specific tasks.
 */
// Default Task
gulp.task( 'default', ['styles', 'customJS',  'images', 'browser-sync'], function () {
    gulp.watch( projectPHPWatchFiles, reload ); // Reload on PHP file changes.
    gulp.watch( styleWatchFiles, [ 'styles' ] ); // Reload on SCSS file changes.
    gulp.watch( customJSWatchFiles, [ 'customJS', reload ] ); // Reload on customJS file changes.
});

// Package Distributable Theme
gulp.task('build', function(cb) {
    runSequence('styles', 'cleanup', 'customJS',  'buildFiles', 'buildImages', 'buildZip','cleanupFinal', cb);
});