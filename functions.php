<?php
/**
 * Paths
 *
 * @since  1.0
 */
if ( !defined( 'WPROCKET_THEME_DIR' ) ){
    define('WPROCKET_THEME_DIR', ABSPATH . 'wp-content/themes/' . get_template());
}

define('THEME_NAME', 'codeable-aero');
define('THEME_VERSION' , '1.0');

/**
 *
 * Require major files
 *
 * @since  1.0.0
 *
 */

$theme_includes = [
    'lib/init.php',                                                // Custom Post Types
    'lib/assets.php',                                                // Custom Post Types
    'lib/cpt.php',                                                // Custom Post Types
    'framework/TGM-Plugin/class-tgm-plugin-activation.php',       // Load TGM Plugin Activation Class
    'framework/TGM-Plugin/config.php',                            // Load TGM Plugin Activation Config File
    'lib/extras.php',                                             // Extras
];

foreach ($theme_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'aenaon'), $file), E_USER_ERROR);
    }

    require_once $filepath;
}
unset($file, $filepath);

