<?php
/**
 *
 * Scripts: Frontend with no conditions, Add Custom Scripts to wp_head
 *
 * @since  1.0.0
 *
 */
add_action('wp_enqueue_scripts', 'aero_scripts');
function aero_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
        //wp_deregister_script('jquery'); // Deregister WordPress jQuery
        wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js', array(), '1.11.2');
        /**
         *
         * Minified and concatenated scripts
         *
         *     @vendors     plugins.min,js
         *     @custom      scripts.min.js
         *
         *     Order is important
         *
         */
        // Register Scripts
        wp_register_script( 'modernizr-js', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array( 'jquery' ), '1.0.0' );
        wp_register_script( 'slick-js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js', array( 'jquery' ), '1.0.0' );
        wp_register_script( 'retina-js', 'https://cdnjs.cloudflare.com/ajax/libs/retina.js/1.3.0/retina.min.js', array( 'jquery' ), '1.0.0' );
        wp_register_script('aero-customJs', get_template_directory_uri() . '/assets/js/custom.min.js'); // Custom scripts


        // Enqueue Scripts
        wp_enqueue_script('jquery');
        wp_enqueue_script('modernizr-js');

        wp_enqueue_script('slick-js');
        wp_enqueue_script('retina-js');
        wp_enqueue_script('aero-customJs');
    }
}


/**
 *
 * Styles: Frontend with no conditions, Add Custom styles to wp_head
 *
 * @since  1.0
 *
 */
add_action('wp_enqueue_scripts', 'aero_styles'); // Add Theme Stylesheet
function aero_styles()
{
    /**
     *
     * Minified and Concatenated styles
     *
     */
    // wp_register_style('aero-style', get_template_directory_uri() . '/style.css', array(), THEME_VERSION, 'all');
    wp_register_style('google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,700|Roboto:400,500,700', array(), THEME_VERSION );
    wp_register_style('aero-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', array(), THEME_VERSION, 'all');
    wp_register_style('aero-slick-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css', array(), THEME_VERSION, 'all');
    wp_register_style('aero-slicktheme-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css', array(), THEME_VERSION, 'all');
    wp_register_style('aero-style', get_template_directory_uri() . '/style.min.css', array(), THEME_VERSION, 'all');

    // wp_enqueue_style('google-fonts'); // Enqueue it!
    wp_enqueue_style('aero-bootstrap'); // Enqueue it!
    wp_enqueue_style('aero-slick-css'); // Enqueue it!
    wp_enqueue_style('aero-slicktheme-css'); // Enqueue it!
    wp_enqueue_style('aero-style'); // Enqueue it!
}
/**
 *
 * Comment Reply js to load only when thread_comments is active
 *
 * @since  1.0.0
 *
 */
add_action( 'wp_enqueue_scripts', 'aero_enqueue_comments_reply' );
function aero_enqueue_comments_reply() {
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}