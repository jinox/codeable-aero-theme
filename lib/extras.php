<?php
// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function aero_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}


// Custom Excerpts
function aero_index($length) // Create 20 Word Callback for Index page Excerpts, call using aerowp_excerpt('aerowp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using aerowp_excerpt('aerowp_custom_post');
function aero_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function aero_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}


