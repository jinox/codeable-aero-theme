<?php

/*
  Custom Post Types
*/

// Register Custom Post Type Clients
$labels = array(
    'name'                  => _x( 'Clients', 'Post Type General Name', THEME_NAME ),
    'singular_name'         => _x( 'Client', 'Post Type Singular Name', THEME_NAME ),
    'menu_name'             => __( 'Clients', THEME_NAME ),
    'name_admin_bar'        => __( 'Clients', THEME_NAME ),
    'archives'              => __( 'Clients Archives', THEME_NAME ),
    'parent_item_colon'     => __( 'Parent client:', THEME_NAME ),
    'all_items'             => __( 'All clients', THEME_NAME ),
    'add_new_item'          => __( 'Add New client', THEME_NAME ),
    'add_new'               => __( 'Add New', THEME_NAME ),
    'new_item'              => __( 'New client', THEME_NAME ),
    'edit_item'             => __( 'Edit client', THEME_NAME ),
    'update_item'           => __( 'Update client', THEME_NAME ),
    'view_item'             => __( 'View client', THEME_NAME ),
    'search_items'          => __( 'Search client', THEME_NAME ),
    'not_found'             => __( 'Not found', THEME_NAME ),
    'not_found_in_trash'    => __( 'Not found in Trash', THEME_NAME ),
    'featured_image'        => __( 'Featured Image', THEME_NAME ),
    'set_featured_image'    => __( 'Set featured image', THEME_NAME ),
    'remove_featured_image' => __( 'Remove featured image', THEME_NAME ),
    'use_featured_image'    => __( 'Use as featured image', THEME_NAME ),
    'insert_into_item'      => __( 'Insert into client', THEME_NAME ),
    'uploaded_to_this_item' => __( 'Uploaded to this client', THEME_NAME ),
    'items_list'            => __( 'Clients list', THEME_NAME ),
    'items_list_navigation' => __( 'Clients list navigation', THEME_NAME ),
    'filter_items_list'     => __( 'Filter clients list', THEME_NAME ),
);
$args = array(
    'label'                 => __( 'Client', THEME_NAME ),
    'description'           => __( 'User and clients', THEME_NAME ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'thumbnail', ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-groups',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => true,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
);
register_post_type( 'clients', $args );


// Register Custom Post Type Testimonials
$labels = array(
    'name'                  => _x( 'Testimonials', 'Post Type General Name', THEME_NAME ),
    'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', THEME_NAME ),
    'menu_name'             => __( 'Testimonials', THEME_NAME ),
    'name_admin_bar'        => __( 'Testimonials', THEME_NAME ),
    'archives'              => __( 'Testimonials Archives', THEME_NAME ),
    'parent_item_colon'     => __( 'Parent testimonial:', THEME_NAME ),
    'all_items'             => __( 'All testimonials', THEME_NAME ),
    'add_new_item'          => __( 'Add New testimonial', THEME_NAME ),
    'add_new'               => __( 'Add New', THEME_NAME ),
    'new_item'              => __( 'New testimonial', THEME_NAME ),
    'edit_item'             => __( 'Edit testimonial', THEME_NAME ),
    'update_item'           => __( 'Update testimonial', THEME_NAME ),
    'view_item'             => __( 'View testimonial', THEME_NAME ),
    'search_items'          => __( 'Search testimonial', THEME_NAME ),
    'not_found'             => __( 'Not found', THEME_NAME ),
    'not_found_in_trash'    => __( 'Not found in Trash', THEME_NAME ),
    'featured_image'        => __( 'Featured Image', THEME_NAME ),
    'set_featured_image'    => __( 'Set featured image', THEME_NAME ),
    'remove_featured_image' => __( 'Remove featured image', THEME_NAME ),
    'use_featured_image'    => __( 'Use as featured image', THEME_NAME ),
    'insert_into_item'      => __( 'Insert into testimonial', THEME_NAME ),
    'uploaded_to_this_item' => __( 'Uploaded to this testimonial', THEME_NAME ),
    'items_list'            => __( 'Testimonials list', THEME_NAME ),
    'items_list_navigation' => __( 'Testimonials list navigation', THEME_NAME ),
    'filter_items_list'     => __( 'Filter testimonials list', THEME_NAME ),
);
$args = array(
    'label'                 => __( 'Testimonial', THEME_NAME ),
    'description'           => __( 'User and client testimonials', THEME_NAME ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'thumbnail', ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-format-status',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => true,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
);
register_post_type( 'testimonials', $args );

// Register Custom Taxonomy FAQ Categories
/*
$labels = array(
    'name'                       => _x( 'Testimonials Categories', 'Taxonomy General Name', THEME_NAME ),
    'singular_name'              => _x( 'Testimonials testimonial', 'Taxonomy Singular Name', THEME_NAME ),
    'menu_name'                  => __( 'Testimonials Categories', THEME_NAME ),
    'all_items'                  => __( 'All Categories', THEME_NAME ),
    'parent_item'                => __( 'Parent testimonial', THEME_NAME ),
    'parent_item_colon'          => __( 'Parent testimonial:', THEME_NAME ),
    'new_item_name'              => __( 'New testimonial', THEME_NAME ),
    'add_new_item'               => __( 'Add New testimonial', THEME_NAME ),
    'edit_item'                  => __( 'Edit testimonial', THEME_NAME ),
    'update_item'                => __( 'Update testimonial', THEME_NAME ),
    'view_item'                  => __( 'View testimonial', THEME_NAME ),
    'separate_items_with_commas' => __( 'Separate testimonials with commas', THEME_NAME ),
    'add_or_remove_items'        => __( 'Add or remove testimonials', THEME_NAME ),
    'choose_from_most_used'      => __( 'Choose from the most used', THEME_NAME ),
    'popular_items'              => __( 'Popular testimonials', THEME_NAME ),
    'search_items'               => __( 'Search testimonials', THEME_NAME ),
    'not_found'                  => __( 'Not Found', THEME_NAME ),
);
$args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
);
register_taxonomy( 'testimonials_category', array( 'testimonials' ), $args );
*/

/*-------------------------------------------------------------------------*/